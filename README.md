# Projeto TASKSB

## Objetivo

Projeto demonstrando o agendamento de tarefa no Springboot.

## Anotações

* @Scheduled(cron=". . .")
* @EnableScheduling

@Scheduled(cron = "00, 10,20,30,40,50 * * * * *")

Executa todos os dias do mês, todos as horas e minutos, sempre a cada 10 segundos 

## Fonte

https://spring.io/guides/gs/scheduling-tasks/#scratch

http://thecodebarbarian.com/node.js-task-scheduling-with-agenda-and-mongodb

http://www.javainuse.com/spring/bootTask

## Padrão para o cron

   field         allowed values
   -----         --------------
   minute        0-59
   hour          0-23
   day of month  1-31
   month         1-12 (or names, see below)
   day of week   0-7 (0 or 7 is Sun, or use names)
   
minutos horas dias meses dias da semana comando

00 8,15 * * * /bin/echo "teste": executa todos os dias da semana, todos os meses, todos os dias do mês, 8h e 15h

http://www.manpagez.com/man/5/crontab/ 