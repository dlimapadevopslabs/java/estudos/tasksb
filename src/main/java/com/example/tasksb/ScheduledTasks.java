package com.example.tasksb;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {
	
	private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);	
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	
	/*
	 * define quando e como o método é chamado
	 * fixedRate: indica o intervalo entre as chamadas
	 * fixedDelay: indica o intervalo entre as chamadas, considerando o momento do encerramento do método
	 * @Scheduled(cron=". . ."): usa expressão regular para o agendamento
	 */
	@Scheduled(fixedRate = 5000)
	public void reportCurrentTime() {
		log.info("Executando a cada 5 segundos usando fixedRate {}", dateFormat.format(new Date()));
	}

	@Scheduled(cron = "00,10,20,30,40,50 * * * * *")
	public void reportCurrentTimeCron() {
		log.info("Executando a cada 10 segundos usando cron = \"00,10,20,30,40,50 * * * * *\" {}", dateFormat.format(new Date()));
	}
}
