package com.example.tasksb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
//
@EnableScheduling
public class TasksbApplication {

	public static void main(String[] args) {
		SpringApplication.run(TasksbApplication.class, args);
	}
}
