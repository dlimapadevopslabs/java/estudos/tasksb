package com.example.tasksb;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.support.CronSequenceGenerator;
import org.springframework.stereotype.Component;

@Component
public class CronSequenceGeneratorTeste {
	
	private static final Logger log = LoggerFactory.getLogger(CronSequenceGeneratorTeste.class);	
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	@Autowired
	public void run() {
		//Execução no próximo dia às 01:00:00 
		CronSequenceGenerator cron1 = new CronSequenceGenerator("0 0 1 * * * ");
		
		//Execução no dia 01/12 às 01:00:00
		CronSequenceGenerator cron2 = new CronSequenceGenerator("0 0 1 1 12 * ");
		
		log.info("Data corrente: {}", dateFormat.format(new Date()));

		log.info("Próximo agendamento pra cron1: {}", cron1.next(new Date()));
		log.info("Próximo agendamento pra cron2: {}", cron2.next(new Date()));
	}
}
